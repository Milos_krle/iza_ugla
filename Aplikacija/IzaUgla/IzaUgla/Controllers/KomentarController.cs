using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IzaUgla.Models;

namespace IzaUgla.Controllers
{
    public class KomentarController : Controller
    {
        private readonly Services.KomentarService komentarService;
        private readonly Services.KorisnikService korisnikService;

        public KomentarController(Services.KomentarService komentarService, Services.KorisnikService korisnikService)
        {
            this.komentarService = komentarService;
            this.korisnikService = korisnikService;
        }
       
        public ActionResult Index()
        {
            return View(komentarService.Get());
        }

       
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var komentar = komentarService.Get(id);
            if (komentar == null)
            {
                return NotFound();
            }
            return View(komentar);
        }

      
        //public ActionResult CreateComment()
        //{
        //    return View();
        //}

        public ActionResult CreateComment(string _id)
        {
            Komentar k = new Komentar();

            if (_id != null)
            {
                k.Id_dogadjaja = _id;
            }
            return View(k);
        }


        [HttpPost]
       
        public ActionResult CreateComment(Komentar komentar)
        {
            //komentar.DatumVreme =
            var korisnici = korisnikService.Get();
            var korisnik = korisnici.Where(korisnik => korisnik.Username == komentar.Username).FirstOrDefault();
            if (korisnik.Password == komentar.Password)
            {
                komentar.Id_korisnika = korisnik.Id;
                komentar.DatumVreme = DateTime.Now;
                if (ModelState.IsValid)
                {
                    komentarService.Create(komentar);
                    return RedirectToAction("Details", "Dogadjaj", new { id = komentar.Id_dogadjaja });
                }
            }
            return View(komentar);
        }
       
        public ActionResult DeleteComment(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var komentar = komentarService.Get(id);
            if (komentar == null)
            {
                return NotFound();
            }
            return View(komentar);
        }

       


        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
               
                var komentar = komentarService.Get(id);

                if (komentar == null)
                {
                    return RedirectToAction("Index", "Dogadjaj");
                }

                komentarService.Remove(komentar.Id);

                return RedirectToAction("Index","Dogadjaj");
            }
            catch
            {
                return RedirectToAction("Index", "Dogadjaj");
            }
        }
    }
}
