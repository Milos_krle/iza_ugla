﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IzaUgla.Models;

namespace IzaUgla.Controllers
{
    public class DogadjajController : Controller
    {
        private readonly Services.DogadjajService dogadjajService;
        private readonly Services.KomentarService komentarService;
        private readonly Services.KorisnikService korisnikService;

        public DogadjajController(Services.DogadjajService dogadjajService,
                                  Services.KomentarService komentarService,
                                  Services.KorisnikService korisnikService)
        {
            this.dogadjajService = dogadjajService;
            this.komentarService = komentarService;
            this.korisnikService = korisnikService;
        }

       
        public ActionResult Index()
        {
            return View(dogadjajService.Get());
        }

       
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dogadjaj = dogadjajService.Get(id);

            if (dogadjaj == null)
            {
                return NotFound();
            }

            var sviKomentari = komentarService.Get();
            var sviKorisnici = korisnikService.Get();
            List<KomentarKorisnik> komentarikorisnika = new List<KomentarKorisnik>();

            foreach(Komentar k in sviKomentari)
            {
                if(k.Id_dogadjaja==dogadjaj.Id)
                {
                    KomentarKorisnik komentarKorisnik = new KomentarKorisnik();
                    komentarKorisnik.komentar = k;
                    var korisnik = sviKorisnici.Where(korisnik => korisnik.Id == k.Id_korisnika).FirstOrDefault();
                    if (korisnik != null)
                        komentarKorisnik.korisnik = korisnik;
                    komentarikorisnika.Add(komentarKorisnik);
                }
            }
            DogadjajKomentarKorisnik dogadjajKomentarKorisnik = new DogadjajKomentarKorisnik();
            dogadjajKomentarKorisnik.dogadjaj = dogadjaj;
            dogadjajKomentarKorisnik.komentarKorisnik = komentarikorisnika;

            return View(dogadjajKomentarKorisnik);
        }

      
        public ActionResult Create()
        {
            return View();
        }

      
        [HttpPost]
       
        public ActionResult Create(Dogadjaj dogadjaj)
        {
            if (ModelState.IsValid)
            {
                dogadjajService.Create(dogadjaj);
                return RedirectToAction(nameof(Index));
            }
            return View(dogadjaj);
        }

      
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dogadjaj = dogadjajService.Get(id);
            if (dogadjaj == null)
            {
                return NotFound();
            }
            return View(dogadjaj);
        }

       
        [HttpPost]
        
        public ActionResult Edit(string id, Dogadjaj dogadjaj)
        {
            if (id != dogadjaj.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                dogadjajService.Update(id, dogadjaj);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(dogadjaj);
            }
        }

       
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dogadjaj = dogadjajService.Get(id);
            if (dogadjaj == null)
            {
                return NotFound();
            }
            return View(dogadjaj);
        }

       
        [HttpPost, ActionName("Delete")]
       
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                var dogadjaj = dogadjajService.Get(id);

                if (dogadjaj == null)
                {
                    return NotFound();
                }

                dogadjajService.Remove(dogadjaj.Id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
