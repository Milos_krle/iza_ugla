﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IzaUgla.Models;

namespace IzaUgla.Controllers
{
    public class KorisnikController : Controller
    {
        private readonly Services.KorisnikService korisnikService;

        public KorisnikController(Services.KorisnikService korisnikService)
        {
            this.korisnikService = korisnikService;
        }

        // GET: KorisnikController
        public ActionResult Index()
        {
            return View(korisnikService.Get());
        }

        // GET: KorisnikController/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var korisnik = korisnikService.Get(id);
            if (korisnik == null)
            {
                return NotFound();
            }
            return View(korisnik);
        }

        // GET: KorisnikController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KorisnikController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Korisnik korisnik)
        {
            if (ModelState.IsValid)
            {
                korisnikService.Create(korisnik);
                return RedirectToAction(nameof(Index));
            }
            return View(korisnik);
        }

        // GET: KorisnikController/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var korisnik = korisnikService.Get(id);
            if (korisnik == null)
            {
                return NotFound();
            }
            return View(korisnik);
        }

        // POST: KorisnikController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, Korisnik korisnik)
        {
            if (id != korisnik.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                korisnikService.Update(id, korisnik);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(korisnik);
            }
        }

        // GET: Cars/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var korisnik = korisnikService.Get(id);
            if (korisnik == null)
            {
                return NotFound();
            }
            return View(korisnik);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            try
            {
                var korisnik = korisnikService.Get(id);

                if (korisnik == null)
                {
                    return NotFound();
                }

                korisnikService.Remove(korisnik.Id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
