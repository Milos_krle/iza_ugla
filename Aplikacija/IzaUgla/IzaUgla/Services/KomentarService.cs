using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using IzaUgla.Models;

namespace IzaUgla.Services
{
    public class KomentarService
    {
        private readonly IMongoCollection<Komentar> _komentari;

        public KomentarService(IConfiguration config)
        {
            MongoClient client = new MongoClient(config.GetConnectionString("IzaUglaDb"));
            IMongoDatabase database = client.GetDatabase("iza_ugla");
            _komentari = database.GetCollection<Komentar>("Komentari");
        }

        public List<Komentar> Get()
        {
            return _komentari.Find(komentar => true).ToList();
        }

        public Komentar Get(string id)
        {
            return _komentari.Find(komentar => komentar.Id == id).FirstOrDefault();
        }

        public Komentar Create(Komentar komentar)
        {
            _komentari.InsertOne(komentar);
            return komentar;
        }        

        public void Remove(string id)
        {
            _komentari.DeleteOne(komentar => komentar.Id == id);
        }
    }
}
