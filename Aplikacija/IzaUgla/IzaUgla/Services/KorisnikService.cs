﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using IzaUgla.Models;

namespace IzaUgla.Services
{
    public class KorisnikService
    {
        private readonly IMongoCollection<Korisnik> korisnici;

        public KorisnikService(IConfiguration config)
        {
            MongoClient client = new MongoClient(config.GetConnectionString("IzaUglaDb"));
            IMongoDatabase database = client.GetDatabase("iza_ugla");
            korisnici = database.GetCollection<Korisnik>("Korisnici");
        }

        public List<Korisnik> Get()
        {
            return korisnici.Find(korisnik => true).ToList();
        }

        public Korisnik Get(string id)
        {
            return korisnici.Find(korisnik => korisnik.Id == id).FirstOrDefault();
        }

        public Korisnik Create(Korisnik korisnik)
        {
            korisnici.InsertOne(korisnik);
            return korisnik;
        }

        public void Update(string id, Korisnik korisnikIn)
        {
            korisnici.ReplaceOne(korisnik => korisnik.Id == id, korisnikIn);
        }

        public void Remove(Korisnik korisnikIn)
        {
            korisnici.DeleteOne(korisnik => korisnik.Id == korisnikIn.Id);
        }

        public void Remove(string id)
        {
            korisnici.DeleteOne(korisnik => korisnik.Id == id);
        }
    }
}
