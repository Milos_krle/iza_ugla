﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using IzaUgla.Models;

namespace IzaUgla.Services
{
    public class DogadjajService
    {
        private readonly IMongoCollection<Dogadjaj> _dogadjaji;

        public DogadjajService(IConfiguration config)
        {
            MongoClient client = new MongoClient(config.GetConnectionString("IzaUglaDb"));
            IMongoDatabase database = client.GetDatabase("iza_ugla");
            _dogadjaji = database.GetCollection<Dogadjaj>("Dogadjaji");
        }

        public List<Dogadjaj> Get()
        {
            return _dogadjaji.Find(dogadjaj => true).ToList();
        }

        public Dogadjaj Get(string id)
        {
            return _dogadjaji.Find(dogadjaj => dogadjaj.Id == id).FirstOrDefault();
        }

        public Dogadjaj Create(Dogadjaj dogadjaj)
        {
            _dogadjaji.InsertOne(dogadjaj);
            return dogadjaj;
        }

        public void Update(string id, Dogadjaj dogId)
        {
            _dogadjaji.ReplaceOne(dogadjaj => dogadjaj.Id == id, dogId);
        }

        public void Remove(Dogadjaj dogadjaj)
        {
            _dogadjaji.DeleteOne(korisnik => korisnik.Id == dogadjaj.Id);
        }

        public void Remove(string id)
        {
            _dogadjaji.DeleteOne(dogadjaj => dogadjaj.Id == id);
        }
    }
}
