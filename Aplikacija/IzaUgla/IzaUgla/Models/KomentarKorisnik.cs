﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IzaUgla.Models;

namespace IzaUgla.Models
{
    public class KomentarKorisnik
    {
        public Komentar komentar { get; set; }
        public Korisnik korisnik { get; set; }
    }
}
