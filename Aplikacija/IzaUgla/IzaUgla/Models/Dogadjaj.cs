﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace IzaUgla.Models
{
    public class Dogadjaj
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Naziv")]
        [Required]
        public string Naziv { get; set; }

        [BsonElement("Opis")]
        [Required]
        public string Opis { get; set; }

        [BsonElement("DatumVreme")]
        [Required]
        public DateTime VremeDatum { get; set; }

        //lokacija??
        [BsonElement("Ulica")]
        [Required]
        public string Ulica { get; set; }
        [BsonElement("Broj")]
 
        public decimal Broj { get; set; }
        [BsonElement("Opstina")]
        [Required]
        public string Opstina { get; set; }
        [BsonElement("Grad")]
        [Required]
        public string Grad { get; set; }




        [BsonElement("Tag")]
        [Display(Name = "Tag")]
        
        public IList<string> TagoviDogadjaja { get; set; }

        //kako povezati sa modelom u bazi
        public IList<Komentar> Komentari { get; set; }


        [BsonElement("ImageUrl")]
        [Display(Name = "Photo")]
        [DataType(DataType.ImageUrl)]
        [Required]
        public string ImageUrl { get; set; }
    }
}
