﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace IzaUgla.Models
{
    public class Komentar
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Id_dogadjaja")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id_dogadjaja { get; set; }


        [BsonElement("Komentar")]
        [Required]
        //sadrzaj komentara
        public string KomentarData { get; set; }

        [BsonElement("Datum_i_vreme")]
        [Required]
        public DateTime DatumVreme { get; set; }

        [BsonElement("Id_korisnika")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id_korisnika {get; set; }

        [BsonElement("Username")]
        [Required]
        public string Username { get; set; }

        [BsonElement("Password")]
        [Required]
        public string Password { get; set; }

       
    }
}
