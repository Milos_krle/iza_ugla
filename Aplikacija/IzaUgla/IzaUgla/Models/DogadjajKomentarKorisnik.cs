﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IzaUgla.Models;

namespace IzaUgla.Models
{
    public class DogadjajKomentarKorisnik
    {
        public Dogadjaj dogadjaj { get; set; }
        public List<KomentarKorisnik> komentarKorisnik { get; set; }
    }
}
